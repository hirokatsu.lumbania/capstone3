import { Button } from 'react-bootstrap';
import Swal from 'sweetalert2';

export default function ActivateProduct({ id, isActive, getAllData }) {
    let status;
    const activate = (id) => {
        fetch(`${process.env.REACT_APP_API_URL}/products/admin/activate/${id}`, {
            method: "PUT",
            headers: {
                "Authorization": `Bearer ${localStorage.getItem('token')}`,
                "Content-Type": "application/json"
            },
            body: JSON.stringify({
                _id: id
            })
        }).then(response => {
            status = response.status;
            return response.json()
        }).then(result => {
            if (status === 200) {
                Swal.fire({
                    title: "Product Activated!",
                    icon: "success",
                    text: result.message
                })
                getAllData()
            } else {
                Swal.fire({
                    title: "Process failed",
                    icon: "error",
                    text: result.message
                })
            }
        })
    }

    const deactivate = (id) => {
        fetch(`${process.env.REACT_APP_API_URL}/products/admin/archive/${id}`, {
            method: "PUT",
            headers: {
                "Authorization": `Bearer ${localStorage.getItem('token')}`,
                "Content-Type": "application/json"
            },
            body: JSON.stringify({
                _id: id
            })
        }).then(response => {
            status = response.status;
            return response.json()
        }).then(result => {
            if (status === 200) {
                Swal.fire({
                    title: "Product Archived!",
                    icon: "success",
                    text: result.message
                })
                getAllData()
            } else {
                Swal.fire({
                    title: "Process failed",
                    icon: "error",
                    text: result.message
                })
            }
        })
    }
    return (
        (isActive === true) ?
            <Button variant="danger" size="sm" onClick={e => deactivate(id)}>Deactivate</Button>
            :
            <Button variant="success" size="sm" onClick={e => activate(id)}>Activate</Button>
    )
}