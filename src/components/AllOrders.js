import { useState, useEffect } from 'react';
import { Card } from 'react-bootstrap';
import Order from './Order.js';

export default function AllOrders() {
    let status;

    const [output, setOutput] = useState([])
    const fetchData = () => {
        fetch(`${process.env.REACT_APP_API_URL}/order/admin/all-orders`, {
            headers: {
                "Authorization": `Bearer ${localStorage.getItem('token')}`
            }
        }).then(response => {
            status = response.status;
            return response.json()
        }).then(result => {
            if (status === 200) {
                if (result.length < 1) {
                    setOutput(
                        <Card>
                            <Card.Body>
                                <Card.Title className='text-center'>
                                    <h2>No Existing Orders</h2>
                                </Card.Title>
                            </Card.Body>
                        </Card>
                    );

                } else {
                    setOutput(result.map(order => {
                        return <Order key={order._id} userId={order.userId} orderId={order._id} products={order.products} date={order.purchasedOn} total={order.totalAmount} fetchData={fetchData} />
                    }))
                }
            } else {
                console.log(result)
            }
        })
    }

    useEffect(() => {
        fetchData();
    }, [])

    return (
        <>
            {output}
        </>

    )
}