import { Link } from 'react-router-dom';
import { Card, Button } from 'react-bootstrap';

export default function ItemCard({ product }) {
    return (
        <>
            <Card className='border border-muted p-4 product-card' >
                <Card.Img variant="top" src={product.image} className='border border-muted' />
                <Card.Body>
                    <Card.Title className='my-3 product-card'><strong>{product.name}</strong></Card.Title>

                    <Card.Subtitle>Price:</Card.Subtitle>
                    <Card.Text>{product.price}</Card.Text>
                    <Button variant="primary" as={Link} to={`/product/${product._id}`}>Details</Button>
                </Card.Body>
            </Card>
        </>
    )
}