import { useState, useEffect, useContext } from 'react';
import { Col, Table } from 'react-bootstrap';
import UserContext from '../UserContext.js';

export default function Order({ userId, orderId, products, date, total, fetchData }) {
    let purchaseDate = date;
    purchaseDate = purchaseDate.split('T');

    const { logIn } = useContext(UserContext);
    const [items, setItems] = useState([]);

    useEffect(() => {

        setItems(products.map(item => {
            return (
                <tr key={item.productId}>
                    <td><img src={item.image} className='cart-image' /></td>
                    <td className='text-start'>{item.name}</td>
                    <td>{item.price}</td>
                    <td>{item.quantity}</td>
                    <td>{item.subtotal}</td>
                </tr>
            )
        }))
        fetchData()
    }, [])

    return (
        <Col lg={12}>
            {
                (logIn.isAdmin === true) ?
                    <>
                        <h6 className='mt-5'>User ID : {userId}</h6>
                        <h6>Order ID : {orderId}</h6>
                    </>
                    :
                    <>
                        <h6 className='mt-5'>Order ID : {orderId}</h6>
                    </>
            }
            <h6>{purchaseDate[0]}</h6>
            <Table className='text-center align-middle mt-3 border border-muted' size="sm" hover responsive>
                <thead>
                    <tr>
                        <th colSpan={2}>Product</th>
                        <th>Price</th>
                        <th>Quantity</th>
                        <th>Subtotal</th>
                    </tr>
                </thead>
                <tbody>
                    {items}
                    <tr>
                        <td colSpan={3} className='h4'>TOTAL</td>
                        <td colSpan={2} className='h4'>{total}</td>
                    </tr>
                </tbody>
            </Table>
        </Col>

    )
}