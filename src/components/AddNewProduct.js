import { useEffect, useState } from 'react';
import { Form, Button } from 'react-bootstrap';
import Swal from 'sweetalert2';

export default function AddNewProduct() {
    let status;

    const [name, setName] = useState("");
    const [description, setDescription] = useState("");
    const [price, setPrice] = useState("");
    const [category, setCategory] = useState("");
    const [image, setImage] = useState("");
    const [isActive, setIsActive] = useState(false);



    const addNewProduct = (e) => {
        e.preventDefault();

        fetch(`${process.env.REACT_APP_API_URL}/products/admin/add-product`, {
            method: "POST",
            headers: {
                "Authorization": `Bearer ${localStorage.getItem('token')}`,
                "Content-Type": "application/json"
            },
            body: JSON.stringify({
                name: name,
                description: description,
                price: price,
                category: category,
                image: image
            })
        }).then(response => {
            status = response.status;
            return response.json()
        }).then(result => {
            if (status === 201) {
                Swal.fire({
                    title: "Product Added!",
                    icon: "success",
                    text: result.message
                })
            } else {
                Swal.fire({
                    title: "Process Failed",
                    icon: "error",
                    text: result.message
                })
            }
        })
        setName("")
        setDescription("")
        setPrice("")
        setCategory("")
        setImage("")
    }

    useEffect(() => {
        if (name !== "" && description !== "" && price !== "" && category !== "" && image !== "") {
            setIsActive(true);
        }
    }, [name, description, price, category, image])

    return (
        <Form onSubmit={e => addNewProduct(e)}>
            <Form.Group>
                <Form.Label>Name</Form.Label>
                <Form.Control type="text"
                    placeholder="Enter Product Name"
                    value={name}
                    onChange={e => setName(e.target.value)}
                    required />
            </Form.Group>
            <Form.Group className='mt-3'>
                <Form.Label>Description</Form.Label>
                <Form.Control type="text"
                    placeholder="Enter Product Description"
                    value={description}
                    onChange={e => setDescription(e.target.value)}
                    required />
            </Form.Group>
            <Form.Group className='mt-3'>
                <Form.Label>Price</Form.Label>
                <Form.Control type="number"
                    placeholder="Enter Price"
                    value={price}
                    onChange={e => setPrice(e.target.value)}
                    required />
            </Form.Group>
            <Form.Group className='mt-3'>
                <Form.Label>Category</Form.Label>
                <Form.Select value={category} onChange={e => setCategory(e.target.value)}>
                    <option value="">Select Category</option>
                    <option value="men">Men</option>
                    <option value="women">Women</option>
                </Form.Select>
            </Form.Group>
            <Form.Group className='mt-3'>
                <Form.Label>Upload Image</Form.Label>
                <Form.Control type="text"
                    placeholder="Enter image link"
                    value={image}
                    onChange={e => setImage(e.target.value)}
                    required />
            </Form.Group>
            {
                (isActive) ?
                    <Button variant="primary" type="submit" className='mt-3'>Submit</Button>
                    :
                    <Button variant="primary" type="submit" disabled className='mt-3'>Submit</Button>
            }
        </Form>
    )
}