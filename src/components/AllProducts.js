import { useState, useEffect } from 'react';
import { Table } from 'react-bootstrap';
import EditProduct from './EditProduct.js';
import ActivateProduct from './ActivateProduct.js';

export default function AllProducts() {
    let status;

    const [products, setProducts] = useState([]);

    const getAllProducts = () => {
        fetch(`${process.env.REACT_APP_API_URL}/products/all-products`)
            .then(response => {
                status = response.status;
                return response.json();
            }).then(result => {
                if (status === 200) {
                    setProducts(result);
                } else {
                    console.log(result.message)
                }
            })
    }

    useEffect(() => {
        getAllProducts();
    }, [])

    return (
        <Table striped bordered hover responsive>
            <thead className='text-center'>
                <tr>
                    <th>id</th>
                    <th>Name</th>
                    <th>Description</th>
                    <th>Category</th>
                    <th>Price</th>
                    <th>Image</th>
                    <th>Availability</th>
                    <th>Actions</th>

                </tr>
            </thead>
            <tbody>
                {products.map(product => {
                    return (
                        <tr key={product._id}>
                            <td>{product._id}</td>
                            <td>{product.name}</td>
                            <td>{product.description}</td>
                            <td>{product.category}</td>
                            <td>{product.price}</td>
                            <td><a href={product.image} target='_blank'>Link</a></td>
                            {
                                (product.isActive === true) ?
                                    <td className='text-success'>Active</td>
                                    :
                                    <td className='text-danger'>Inactive</td>
                            }
                            <td ><EditProduct id={product._id} getAllData={getAllProducts} /> <ActivateProduct id={product._id} isActive={product.isActive} getAllData={getAllProducts} /></td>
                        </tr>
                    )
                })}
            </tbody>
        </Table>
    )
}