import { Button } from 'react-bootstrap';
import Swal from 'sweetalert2';

export default function CartItem({ cartItem, fetchData }) {

    let status;
    const addOne = (id) => {
        fetch(`${process.env.REACT_APP_API_URL}/cart/add-one`, {
            method: "PUT",
            headers: {
                "Authorization": `Bearer ${localStorage.getItem('token')}`,
                "Content-Type": "application/json"
            },
            body: JSON.stringify({
                _id: id
            })
        }).then(response => {
            return response.json()
        }).then(result => {
            fetchData();
        })
    }

    const subtractOne = (id) => {
        fetch(`${process.env.REACT_APP_API_URL}/cart/subtract-one`, {
            method: "PUT",
            headers: {
                "Authorization": `Bearer ${localStorage.getItem('token')}`,
                "Content-Type": "application/json"
            },
            body: JSON.stringify({
                _id: id
            })
        }).then(response => {
            return response.json()
        }).then(result => {
            fetchData();
        })
    }

    const removeItem = (id) => {
        fetch(`${process.env.REACT_APP_API_URL}/cart/remove-item`, {
            method: "DELETE",
            headers: {
                "Authorization": `Bearer ${localStorage.getItem('token')}`,
                "Content-Type": "application/json"
            },
            body: JSON.stringify({
                _id: id
            })
        }).then(response => {
            status = response.status;
            return response.json()
        }).then(result => {
            if (status === 200) {
                Swal.fire({
                    title: "Item removed from cart!",
                    icon: "success",
                })
                fetchData();
            }
        })

    }


    return (
        <tr>
            <td><img src={cartItem.image} className='cart-image mx-auto border border-muted' /></td>
            <td className='text-start'>{cartItem.name}</td>
            <td>{cartItem.price}</td>
            <td>

                {
                    (cartItem.quantity === 1) ?
                        <Button variant="success" size="sm" onClick={() => subtractOne(cartItem.productId)} disabled>-</Button>
                        :
                        <Button variant="success" size="sm" onClick={() => subtractOne(cartItem.productId)}>-</Button>
                }
            </td>
            <td>{cartItem.quantity}</td>
            <td><Button variant="success" size="sm" onClick={() => addOne(cartItem.productId)}>+</Button></td>
            <td>{cartItem.subtotal}</td>
            <td><Button variant="danger" size="sm" onClick={() => removeItem(cartItem.productId)}>Remove</Button></td>
        </tr>

    )
}