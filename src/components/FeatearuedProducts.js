import { useState, useEffect } from 'react';
import { Col } from 'react-bootstrap';
import ItemCard from './ItemCard.js';

export default function FeaturedProducts({ category }) {

    const [selectedItems, setSelectedItems] = useState([]);

    const fetchData = () => {
        let filteredData = [];
        fetch(`${process.env.REACT_APP_API_URL}/products/all-products/active`)
            .then(response => response.json())
            .then(result => {
                filteredData = result.filter(products => {
                    return products.category === category
                })
                const numbers = [];
                const featured = []

                const generateRandomNums = () => {
                    let randomNum = Math.floor(Math.random() * filteredData.length)

                    if (numbers.indexOf(randomNum) === -1) {
                        numbers.push(randomNum);
                    } else {
                        generateRandomNums();
                    }
                };

                for (let i = 0; i < 4; i++) {
                    generateRandomNums();
                    featured.push(
                        <Col lg={3} xs={12} className='my-3' key={filteredData[numbers[i]]._id}>
                            <ItemCard product={filteredData[numbers[i]]} />
                        </Col>

                    )
                }
                setSelectedItems(featured);
            })
    }
    useEffect(() => {
        fetchData();
    }, [])


    return (
        <>

            {selectedItems}

        </>

    )

}