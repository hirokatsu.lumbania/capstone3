import { useContext } from 'react';
import { Navbar, Nav, NavDropdown } from 'react-bootstrap';
import { NavLink, Link } from 'react-router-dom';
import UserContext from '../UserContext.js';

export default function AppNavBar() {

    const { logIn } = useContext(UserContext);

    return (
        <Navbar bg="light" expand="lg" className='border-bottom border-dark navbar'>
            <Navbar.Brand as={Link} to="/" className='ms-3'>.hiro</Navbar.Brand>
            <Navbar.Toggle aria-controls="basic-navbar-nav" />
            <Navbar.Collapse id="basic-navbar-nav">
                <Nav className="ms-auto">
                    <Nav.Link as={NavLink} to="/">.home</Nav.Link>
                    {
                        (logIn.id === null) ?
                            <>
                                <NavDropdown title=".shop" id="basic-nav-dropdown">
                                    <NavDropdown.Item as={NavLink} to="/men">.men</NavDropdown.Item>
                                    <NavDropdown.Item as={NavLink} to="/women">.women</NavDropdown.Item>
                                </NavDropdown>
                                <Nav.Link as={NavLink} to="/login">.login</Nav.Link>
                                <Nav.Link as={NavLink} to="/register" className='me-3'>.register</Nav.Link>
                            </>
                            :
                            (logIn.isAdmin === true) ?
                                <>
                                    <Nav.Link as={NavLink} to="/dashboard">.dashboard</Nav.Link>
                                    <Nav.Link as={NavLink} to="/logout" className='me-3'>.logout</Nav.Link>
                                </>
                                :
                                <>
                                    <NavDropdown title=".shop" id="basic-nav-dropdown">
                                        <NavDropdown.Item as={NavLink} to="/men">.men</NavDropdown.Item>
                                        <NavDropdown.Item as={NavLink} to="/women">.women</NavDropdown.Item>
                                    </NavDropdown>
                                    <Nav.Link as={NavLink} to="/profile">.profile</Nav.Link>
                                    <Nav.Link as={NavLink} to="/cart">.cart</Nav.Link>
                                    <Nav.Link as={NavLink} to="/logout" className='me-3'>.logout</Nav.Link>
                                </>
                    }
                </Nav>
            </Navbar.Collapse>
        </Navbar>
    )
}