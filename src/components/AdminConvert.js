import { useState, useEffect } from 'react';
import { Form, Button } from 'react-bootstrap';
import Swal from 'sweetalert2';

export default function AdminConvert() {

    let status;
    const [email, setEmail] = useState("");
    const [isActive, setIsActive] = useState(false);

    const adminConvert = (e) => {
        e.preventDefault();
        fetch(`${process.env.REACT_APP_API_URL}/user/admin/convert`, {
            method: "PUT",
            headers: {
                "Authorization": `Bearer ${localStorage.getItem('token')}`,
                "Content-Type": "application/json"
            },
            body: JSON.stringify({
                email: email
            })
        }).then(response => {
            status = response.status;
            return response.json()
        })
            .then(result => {
                if (status === 200) {
                    Swal.fire({
                        title: "User set to Admin!",
                        icon: "success",
                        text: result.message
                    })
                } else {
                    Swal.fire({
                        title: "Unable to update user details",
                        icon: "error",
                        text: result.message
                    })
                }
            })
        setEmail("");
    }

    useEffect(() => {
        if (email !== "") {
            setIsActive(true);
        }
    }, [email])

    return (
        <Form onSubmit={e => { adminConvert(e) }}>
            <Form.Group>
                <Form.Label>Please enter Email Address of user to be converted to Admin</Form.Label>
                <Form.Control type="email"
                    placeholder="Enter email"
                    value={email}
                    onChange={e => setEmail(e.target.value)}
                    required
                />
            </Form.Group>
            {
                (isActive === true) ?
                    <Button variant="primary" type="submit" className='mt-3'>Submit</Button>
                    :
                    <Button variant="primary" type="submit" disabled className='mt-3'>Submit</Button>
            }
        </Form>
    )
}