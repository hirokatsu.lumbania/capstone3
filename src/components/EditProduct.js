import { useState } from 'react';
import { Button, Form, Modal } from 'react-bootstrap';
import Swal from 'sweetalert2';

export default function EditProduct({ id, getAllData }) {

    let status;
    const [show, setShow] = useState(false)
    const [name, setName] = useState("");
    const [description, setDescription] = useState("");
    const [price, setPrice] = useState("");
    const [category, setCategory] = useState("");
    const [image, setImage] = useState("");

    const openEdit = (id) => {
        fetch(`${process.env.REACT_APP_API_URL}/products/${id}`)
            .then(response => response.json())
            .then(result => {
                setName(result.name);
                setDescription(result.description);
                setPrice(result.price);
                setCategory(result.category);
                setImage(result.image);
            })
        setShow(true);
    }
    const closeEdit = () => {
        setShow(false);
        setName("");
        setDescription("");
        setPrice("");
        setCategory("");
        setImage("");
    }

    const editProduct = (e, id) => {
        e.preventDefault();

        fetch(`${process.env.REACT_APP_API_URL}/products/admin/update-product/${id}`, {
            method: "PUT",
            headers: {
                "Authorization": `Bearer ${localStorage.getItem('token')}`,
                "Content-Type": "application/json"
            },
            body: JSON.stringify({
                name: name,
                description: description,
                price: price,
                category: category,
                image: image
            })
        }).then(response => {
            status = response.status;
            return response.json()
        }).then(result => {
            if (status === 200) {
                Swal.fire({
                    title: "Product updated!",
                    icon: "success",
                    text: result.message
                })
                getAllData();
            } else {
                Swal.fire({
                    title: "Unable to update product.",
                    icon: "error",
                    text: result.message
                })
            }
            closeEdit();
        })
    }

    return (
        <>
            <Button variant="primary" size="sm" onClick={() => openEdit(id)}>
                Edit
            </Button>
            <Modal show={show} onHide={closeEdit}>
                <Form onSubmit={e => editProduct(e, id)}>
                    <Modal.Header closeButton>
                        <Modal.Title>Edit Product Details</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        <Form.Group>
                            <Form.Label>Name</Form.Label>
                            <Form.Control type='text'
                                value={name}
                                onChange={e => setName(e.target.value)}
                                required />
                        </Form.Group>
                        <Form.Group>
                            <Form.Label>Description</Form.Label>
                            <Form.Control type='text'
                                value={description}
                                onChange={e => setDescription(e.target.value)}
                                required />
                        </Form.Group>
                        <Form.Group>
                            <Form.Label>Price</Form.Label>
                            <Form.Control type='number'
                                value={price}
                                onChange={e => setPrice(e.target.value)}
                                required />
                        </Form.Group>
                        <Form.Group>
                            <Form.Label>Category</Form.Label>
                            <Form.Select value={category} onChange={e => setCategory(e.target.value)}>
                                <option value="">Select Category</option>
                                <option value="men">Men</option>
                                <option value="women">Women</option>
                            </Form.Select>
                        </Form.Group>
                        <Form.Group>
                            <Form.Label>Upload Image</Form.Label>
                            <Form.Control type="text"
                                placeholder="Enter image link"
                                value={image}
                                onChange={e => setImage(e.target.value)}
                                required />
                        </Form.Group>
                    </Modal.Body>
                    <Modal.Footer>
                        <Button variant="secondary" onClick={closeEdit}>
                            Close
                        </Button>
                        <Button variant="primary" type="submit">
                            Save Changes
                        </Button>
                    </Modal.Footer>
                </Form>
            </Modal>
        </>
    )
}