import { Container } from 'react-bootstrap';
import { useState, useEffect } from 'react';
import './App.css';
import { BrowserRouter as Router } from 'react-router-dom';
import { Routes, Route } from 'react-router-dom';
import AppNavBar from './components/AppNavBar.js';
import Home from './pages/Home.js';
import Login from './pages/Login.js';
import Logout from './pages/Logout.js';
import Register from './pages/Register.js';
import Error from './pages/Error.js';
import AdminDashboard from './pages/AdminDashboard.js';
import MenProducts from './pages/MenProducts.js';
import WomenProducts from './pages/WomenProducts.js';
import ProductView from './pages/ProductView.js';
import Cart from './pages/Cart.js';
import Checkout from './pages/Checkout';
import Profile from './pages/Profile.js';
import { UserProvider } from './UserContext.js';

function App() {

  document.body.style = 'background: #FAF9F6;'

  const [logIn, setLogIn] = useState({
    id: null,
    isAdmin: null,
    address: null,
    firstname: null,
    lastName: null,
    mobileNo: null
  });

  const logOut = () => {
    localStorage.clear();
  }

  useEffect(() => {
    if (localStorage.getItem('token') !== null) {
      fetch(`${process.env.REACT_APP_API_URL}/user/details`, {
        headers: {
          "Authorization": `Bearer ${localStorage.getItem('token')}`
        }
      }).then(response => response.json())
        .then(result => {
          setLogIn({
            id: result._id,
            isAdmin: result.isAdmin,
            address: result.address,
            firstName: result.firstName,
            lastName: result.lastName,
            mobileNo: result.mobileNo
          })
        })
    }
  }, [])


  return (
    <UserProvider value={{ logIn, setLogIn, logOut }}>
      <Router>
        <AppNavBar />
        <Container>
          <Routes>
            <Route path="/" element={<Home />} />
            <Route path="/login" element={<Login />} />
            <Route path="/logout" element={<Logout />} />
            <Route path="/register" element={<Register />} />
            <Route path="/profile" element={<Profile />} />
            <Route path="/dashboard" element={<AdminDashboard />} />
            <Route path="/men" element={<MenProducts />} />
            <Route path="/women" element={<WomenProducts />} />
            <Route path="/cart" element={<Cart />} />
            <Route path="/product/:productId" element={<ProductView />} />
            <Route path="/checkout" element={<Checkout />} />
            <Route path="*" element={<Error />} />
          </Routes>
        </Container>
      </Router>
    </UserProvider>
  );
}

export default App;
