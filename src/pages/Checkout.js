import { useContext, useState, useEffect } from 'react';
import { Table, Row, Col, Card, Form, Button } from 'react-bootstrap';
import { Navigate, Link } from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';

export default function Checkout() {
    let status;

    const { logIn } = useContext(UserContext);
    const [items, setItems] = useState([]);
    const [total, setTotal] = useState("");

    const checkout = () => {
        fetch(`${process.env.REACT_APP_API_URL}/order/checkout`, {
            method: "POST",
            headers: {
                Authorization: `Bearer ${localStorage.getItem('token')}`
            }
        }).then(response => {
            status = response.status;
            return response.json()
        }).then(result => {
            if (status === 201) {
                Swal.fire({
                    title: "Successfully placed order!",
                    icon: "success",
                    text: `Your order ID is ${result._id}`
                })
            } else {
                Swal.fire({
                    title: "Unable to place order",
                    icon: "error",
                    text: result.message
                })
            }
        })
    }

    const itemsToPurchase = () => {
        if (logIn.id !== null && logIn.isAdmin !== true) {
            fetch(`${process.env.REACT_APP_API_URL}/cart/subtotal`, {
                headers: {
                    "Authorization": `Bearer ${localStorage.getItem('token')}`
                }
            }).then(response => response.json())
                .then(result => {
                    setItems(result.items.map(item => {
                        return (
                            <tr key={item.productId}>
                                <td><img src={item.image} className='cart-image' /></td>
                                <td className='text-start'>{item.name}</td>
                                <td>{item.price}</td>
                                <td>{item.quantity}</td>
                                <td>{item.subtotal}</td>
                            </tr>
                        )
                    }))
                })
        }
    }

    const getTotal = () => {
        fetch(`${process.env.REACT_APP_API_URL}/cart/total`, {
            headers: {
                "Authorization": `Bearer ${localStorage.getItem('token')}`
            }
        }).then(response => response.json())
            .then(result => {
                setTotal(result.total);
            })
    }

    useEffect(() => {
        itemsToPurchase();
        getTotal();
    }, [])

    return (
        (logIn.id !== null && logIn.isAdmin === false) ?
            <>
                <h1 className='text-center mt-5'>.checkout</h1>
                <Row>
                    <Col lg={8}>
                        <h2 className='mt-5'>Order Summary</h2>
                        <Table className='text-center align-middle mt-3 border border-muted' size="sm" hover responsive>
                            <thead>
                                <tr>
                                    <th colSpan={2}>Product</th>
                                    <th>Price</th>
                                    <th>Quantity</th>
                                    <th>Subtotal</th>
                                </tr>
                            </thead>
                            <tbody>
                                {items}
                                <tr>
                                    <td colSpan={3} className='h4'>TOTAL</td>
                                    <td colSpan={2} className='h4'>{total}</td>
                                </tr>
                            </tbody>
                        </Table>
                    </Col>
                    <Col lg={4}>
                        <h2 className='mt-5'>Customer Details</h2>
                        <Card className='mt-3'>
                            <Card.Body>
                                <Card.Subtitle>Full Name</Card.Subtitle>
                                <Card.Title>{logIn.firstName} {logIn.lastName}</Card.Title>
                                <Card.Subtitle className='mt-3'>Address</Card.Subtitle>
                                <Card.Title>{logIn.address}</Card.Title>
                                <Card.Subtitle className='mt-3'>Mobile Number</Card.Subtitle>
                                <Card.Title>{logIn.mobileNo}</Card.Title>
                                <Card.Subtitle className='mt-3'>Shipping Options</Card.Subtitle>
                                <Form className='mt-2'>
                                    <div key={`default-radio`}>
                                        <Form.Check type={`radio`} name="courier" id={'courier-1'} label={`Goat (15-30 business days)`} />
                                        <Form.Check type={`radio`} name="courier" id={'courier-2'} label={`Horse (7-10 business days)`} />
                                        <Form.Check type={`radio`} name="courier" id={'courier-3'} label={`Drone (3-5 business days)`} />
                                    </div>
                                </Form>
                                <Card.Subtitle className='mt-3'>Payment Options</Card.Subtitle>
                                <Form className='mt-2'>
                                    <div key={`default-radio`}>
                                        <Form.Check type={`radio`} name="payment" id={'payment-1'} label={`Cash on Delivery`} />
                                        <Form.Check type={`radio`} name="payment" id={'payment-2'} label={`GCASH`} />
                                        <Form.Check type={`radio`} name="payment" id={'payment-3'} label={`Debit/Credit Card`} />
                                    </div>
                                </Form>
                                <div className="d-grid gap-2 mt-5">
                                    <Button variant="primary" size="lg" onClick={() => checkout()} as={Link} to="/">
                                        Place Order
                                    </Button>
                                </div>
                            </Card.Body>
                        </Card>
                    </Col>
                </Row>
            </>
            :
            <Navigate to="/" />

    )
}