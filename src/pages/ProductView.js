import { useEffect, useState, useContext } from 'react';
import { Card, Row, Col, Button } from 'react-bootstrap';
import { useParams, Link } from "react-router-dom";
import Swal from 'sweetalert2';
import UserContext from '../UserContext.js';

export default function ProductView() {
    let status;
    const { logIn } = useContext(UserContext);
    const { productId } = useParams();
    const [details, setDetails] = useState("");

    const addToCart = (id) => {
        fetch(`${process.env.REACT_APP_API_URL}/cart/add-item`, {
            method: "PUT",
            headers: {
                "Authorization": `Bearer ${localStorage.getItem('token')}`,
                "Content-Type": "application/json"
            },
            body: JSON.stringify({
                _id: id,
                quantity: 1
            })
        }).then(response => {
            status = response.status;
            return response.json()
        }).then(result => {
            if (status === 200) {
                Swal.fire({
                    title: "Item added to Cart!",
                    icon: "success"
                })
            } else {
                Swal.fire({
                    title: "Unable to add item to cart",
                    icon: "error",
                    text: result.message
                })
            }
        })
    }

    useEffect(() => {
        fetch(`${process.env.REACT_APP_API_URL}/products/${productId}`)
            .then(response => {
                status = response.status;
                return response.json()
            }).then(result => {
                if (status === 200) {
                    setDetails(result);
                } else {
                    console.log(result.message)
                }
            })
    }, [])

    return (
        <>
            <Row className='mt-5'>
                <Col lg={{ span: 5, offset: 1 }}>
                    <Card>
                        <Card.Img variant="top" src={details.image} />
                    </Card>
                </Col>
                <Col lg={5}>
                    <Card>

                        <Card.Body>
                            <Card.Title className='my-3 product-card'><strong>{details.name}</strong></Card.Title>
                            <Card.Subtitle>Description:</Card.Subtitle>
                            <Card.Text>{details.description}</Card.Text>
                            <Card.Subtitle>Price:</Card.Subtitle>
                            <Card.Text>{details.price}</Card.Text>
                            {
                                (logIn.id !== null) ?
                                    (logIn.isAdmin === false) ?
                                        <Button variant='primary' onClick={() => addToCart(productId)} as={Link} to={`/${details.category}`}>Add to Cart</Button>
                                        :
                                        <Button variant='primary' disabled>Please switch to Customer account</Button>
                                    :
                                    <Button variant='primary' as={Link} to={`/login`}>Log In to purchase</Button>
                            }

                        </Card.Body>
                    </Card>
                    <Card className='mt-3'>

                        <Card.Body>
                            <Card.Title className='my-3 product-card'><strong>Product Reviews</strong></Card.Title>
                            <Card className='mt-2'>
                                <Card.Body>
                                    <Card.Subtitle><strong>FakeReviewer</strong></Card.Subtitle>
                                    <Card.Text>Fireee</Card.Text>
                                </Card.Body>
                            </Card>
                            <Card className='mt-2'>
                                <Card.Body>
                                    <Card.Subtitle><strong>FakeReviewer2</strong></Card.Subtitle>
                                    <Card.Text>Sheesh</Card.Text>
                                </Card.Body>
                            </Card>
                            <Card className='mt-2'>
                                <Card.Body>
                                    <Card.Subtitle><strong>FakeReviewer3</strong></Card.Subtitle>
                                    <Card.Text>Good</Card.Text>
                                </Card.Body>
                            </Card>
                        </Card.Body>
                    </Card>
                </Col>
            </Row>


        </>


    )
}