import { useState, useEffect, useContext } from 'react';
import { Form, Button, Row, Col } from 'react-bootstrap';
import { Navigate } from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext.js';

export default function Login() {
    const { logIn, setLogIn } = useContext(UserContext);
    const [email, setEmail] = useState("");
    const [password, setPassword] = useState("");
    const [isActive, setIsActive] = useState(false);

    const authenticate = (e) => {
        e.preventDefault();
        fetch(`${process.env.REACT_APP_API_URL}/user/login`, {
            method: "POST",
            headers: {
                "Content-Type": "application/json"
            },
            body: JSON.stringify({
                email: email,
                password: password
            })
        }).then(response => response.json())
            .then(result => {
                if (result.token) {
                    localStorage.setItem('token', result.token);
                    retrieveUserDetails(result.token);
                    Swal.fire({
                        title: "Log In Success!",
                        icon: "success",
                        text: result.message
                    })
                } else {
                    Swal.fire({
                        title: "Log In Failed",
                        icon: "error",
                        text: result.message
                    })
                }
            })
        setEmail("");
        setPassword("");
    }

    const retrieveUserDetails = (token) => {
        fetch(`${process.env.REACT_APP_API_URL}/user/details`, {
            headers: {
                "Authorization": `Bearer ${token}`
            }
        })
            .then(response => response.json())
            .then(result => {
                setLogIn({
                    id: result._id,
                    isAdmin: result.isAdmin,
                    address: result.address,
                    firstName: result.firstName,
                    lastName: result.lastName,
                    mobileNo: result.mobileNo
                })
            })
    }

    useEffect(() => {
        if (email !== "" && password !== "") {
            setIsActive(true);
        }
    }, [email, password])

    return (
        (logIn.id === null) ?
            <Row className='py-5 border border-dark w-75 mx-auto mt-5 text-center box-shadow'>
                <h2 className='text-center my-5'>LOGIN</h2>
                <Col lg={{ span: 6, offset: 3 }}>
                    <Form onSubmit={e => authenticate(e)} className='mt-3'>
                        <Form.Group className='mt-2'>
                            <Form.Control type="email"
                                placeholder="Enter email"
                                value={email}
                                onChange={e => setEmail(e.target.value)}
                                required
                            />
                        </Form.Group>
                        <Form.Group className='mt-2'>
                            <Form.Control type="password"
                                placeholder="Password"
                                value={password}
                                onChange={e => setPassword(e.target.value)}
                                required />
                        </Form.Group>
                        {
                            (isActive) ?
                                <Button variant="primary" type="submit" className='mt-4'>
                                    Submit
                                </Button>
                                :
                                <Button variant="primary" type="submit" disabled className='mt-4'>
                                    Submit
                                </Button>
                        }
                    </Form>
                    <h6 className='my-5'>Don't have an account yet? <a href="/register">Register Here</a></h6>
                </Col>
            </Row>
            :
            <Navigate to="/" />
    )
}