import { useState, useContext, useEffect } from 'react';
import { Row, Col, Card, Button } from 'react-bootstrap';
import {Navigate} from 'react-router-dom';
import UserContext from '../UserContext.js';
import Order from '../components/Order.js';

export default function Profile() {
    let status;
    const { logIn } = useContext(UserContext);
    const [orders, setOrders] = useState([])
    const [output, setOutput] = useState([])
    const [isShown, setIsShown] = useState(false);

    const fetchData = () => {
        fetch(`${process.env.REACT_APP_API_URL}/order/my-orders`, {
            headers: {
                "Authorization": `Bearer ${localStorage.getItem('token')}`
            }
        }).then(response => {
            status = response.status;
            return response.json()
        })
            .then(result => {
                if (status === 200) {
                    setOrders(result)
                } else {
                    console.log(result);
                }
            })


    }
    const myOrders = () => {
        if (orders.length < 1) {
            setOutput(
                <Card>
                    <Card.Body>
                        <Card.Title className='text-center'>
                        <h2>No Existing Orders</h2>
                        </Card.Title>
                    </Card.Body>
                </Card>
            );

        } else {
            setOutput(orders.map(order => {
                return <Order key={order._id} userId = {order.userId} orderId={order._id} products={order.products} date={order.purchasedOn} total={order.totalAmount} fetchData={fetchData} />
            }))
        }
        setIsShown(true);

    }

    const hideOrders = () => {
        setIsShown(false);
        setOutput([]);
    }
    useEffect(() => {
        fetchData();
    }, [])


    return (
        (logIn.isAdmin === false) ?
        <>
            <Row className='mt-5'>
                <Col lg={{ span: 8, offset: 2 }}>
                    <Card>
                        <Card.Header><Card.Title>Profile</Card.Title></Card.Header>
                        <Card.Body>
                            <Card.Subtitle>Full Name</Card.Subtitle>
                            <Card.Title>{logIn.firstName} {logIn.lastName}</Card.Title>
                            <Card.Subtitle className='mt-3'>Address</Card.Subtitle>
                            <Card.Title>{logIn.address}</Card.Title>
                            <Card.Subtitle className='mt-3'>Mobile Number</Card.Subtitle>
                            <Card.Title>{logIn.mobileNo}</Card.Title>
                        </Card.Body>
                    </Card>
                </Col>
            </Row>
            <Row>
                <Col lg={{ span: 8, offset: 2 }}>
                    {(isShown) ?
                        <div className="d-grid gap-2 mt-5">
                            <Button variant="primary" size="md" onClick={() => hideOrders()}>
                                Hide Previous Orders
                            </Button>
                        </div>
                        :
                        <div className="d-grid gap-2 mt-5">
                            <Button variant="primary" size="md" onClick={() => myOrders()}>
                                Show Previous Orders
                            </Button>
                        </div>
                    }

                    {output}
                </Col>
            </Row>
        </>
        :
        <Navigate to ="/dashboard" />
    )
}