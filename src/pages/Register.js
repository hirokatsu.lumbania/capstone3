import { useState, useEffect, useContext } from 'react';
import { Form, Button, Row, Col } from 'react-bootstrap';
import { Navigate } from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext.js';

export default function Register() {
    const { logIn } = useContext(UserContext);

    const [firstName, setFirstName] = useState("");
    const [lastName, setLastName] = useState("");
    const [email, setEmail] = useState("");
    const [mobileNo, setMobileNo] = useState("");
    const [address, setAddress] = useState("");
    const [password, setPassword] = useState("");
    const [confirmPassword, setConfirmPassword] = useState("");
    const [isActive, setIsActive] = useState(false);

    let status;


    const newAccount = e => {
        e.preventDefault();

        fetch(`${process.env.REACT_APP_API_URL}/user/register`, {
            method: "POST",
            headers: {
                "Content-Type": "application/json"
            },
            body: JSON.stringify({
                firstName: firstName,
                lastName: lastName,
                mobileNo: mobileNo,
                email: email,
                address: address,
                password: password
            })
        }).then(response => {
            status = response.status;
            return response.json()
        })
            .then(result => {
                if (status === 201) {
                    Swal.fire({
                        title: "Registration Complete!",
                        icon: "success",
                        text: result.message
                    })
                } else {
                    Swal.fire({
                        title: "Unable to register user",
                        icon: "error",
                        text: result.message
                    })
                }
            })
    }

    useEffect(() => {
        if (firstName !== "" && lastName !== "" && mobileNo !== "" && email !== "" && password !== "" & confirmPassword !== "" && address !== "" && (password === confirmPassword)) {
            setIsActive(true);
        }
    }, [firstName, lastName, email, mobileNo, address, password, confirmPassword])
    return (
        (logIn.id === null) ?

            <Row className='py-5 border border-dark w-75 mx-auto mt-5 text-center box-shadow'>
                <h2 className='text-center my-5'>ELEVATE YOUR STYLE</h2>
                <Col lg={{ span: 6, offset: 3 }}>
                    <Form onSubmit={e => newAccount(e)} className='mt-3'>
                        <Form.Group className='mt-2'>
                            <Form.Control type="text"
                                placeholder="First Name"
                                value={firstName}
                                onChange={e => setFirstName(e.target.value)}
                                required
                            />
                        </Form.Group>
                        <Form.Group className='mt-2'>
                            <Form.Control type="text"
                                placeholder="Last Name"
                                value={lastName}
                                onChange={e => setLastName(e.target.value)}
                                required
                            />
                        </Form.Group>
                        <Form.Group className='mt-2'>
                            <Form.Control type="email"
                                placeholder="Email Address"
                                value={email}
                                onChange={e => setEmail(e.target.value)}
                                required
                            />
                        </Form.Group>
                        <Form.Group className='mt-2'>
                            <Form.Control type="text"
                                placeholder="Mobile Number"
                                value={mobileNo}
                                onChange={e => setMobileNo(e.target.value)}
                                required
                            />
                        </Form.Group>
                        <Form.Group className='mt-2'>
                            <Form.Control type="text"
                                placeholder="Address"
                                value={address}
                                onChange={e => setAddress(e.target.value)}
                                required
                            />
                        </Form.Group>
                        <Form.Group className='mt-2'>
                            <Form.Control type="password"
                                placeholder="Password"
                                value={password}
                                onChange={e => setPassword(e.target.value)}
                                required
                            />
                        </Form.Group>
                        <Form.Group className='mt-2'>
                            <Form.Control type="password"
                                placeholder="Confirm Password"
                                value={confirmPassword}
                                onChange={e => setConfirmPassword(e.target.value)}
                                required
                            />
                        </Form.Group>
                        {
                            (isActive) ?
                                <Button variant="primary" type="submit" size='lg' className='mt-3'>
                                    Submit
                                </Button>
                                :
                                <Button variant="primary" type="submit" disabled size='lg' className='mt-3'>
                                    Submit
                                </Button>
                        }
                    </Form>
                    <h6 className='my-5'>Already have an account? <a href="/login">Login Here</a></h6>
                </Col>
            </Row>

            :
            <Navigate to="/" />
    )
}