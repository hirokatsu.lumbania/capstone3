import { useContext, useEffect } from 'react';
import { Navigate } from 'react-router-dom';

import UserContext from '../UserContext.js';

export default function Logout() {
    const { setLogIn, logOut } = useContext(UserContext);

    useEffect(() => {
        setLogIn({
            id: null,
            isAdmin: null,
            address: null,
            firstname: null,
            lastName: null,
            mobileNo: null
        })
        logOut();
    }, [])

    return (
        <Navigate to="/login" />
    )
}