import { Row, Col } from 'react-bootstrap';

import FeaturedProducts from '../components/FeatearuedProducts';
export default function Home() {
    return (
        <>
            <Row className='home-page text-white'>
                <Col className='text-center my-5'>
                    <h1 className='my-5'><strong>Elevate your Style</strong></h1>
                    <h3>Style Yourself</h3>
                    <h3>Unveil Your Authenticity</h3>
                    <h3>Elevate Every Day</h3>
                    <h3 className='mt-5'>Discover Your Perfect Apparel at </h3>
                    <h2><strong>.hiro</strong></h2>
                </Col>
            </Row>
            <Row className='my-5'>
                <h4>Hot Products for Men</h4>
                <FeaturedProducts category={"men"} />
            </Row>
            <Row className='my-5'>
                <h4>Hot Products for Women</h4>
                <FeaturedProducts category={"women"} />
            </Row>
        </>



    )
}