import { useContext } from 'react';
import { Accordion } from 'react-bootstrap';
import { Navigate } from 'react-router-dom';
import UserContext from '../UserContext.js';
import AdminConvert from '../components/AdminConvert.js';
import AddNewProduct from '../components/AddNewProduct.js';
import AllProducts from '../components/AllProducts.js';
import AllOrders from '../components/AllOrders.js';

export default function AdminDashboard() {
    const { logIn } = useContext(UserContext);
    return (
        (logIn.isAdmin === true) ?
            <Accordion className='mt-5'>
                <Accordion.Item eventKey="0">
                    <Accordion.Header>Convert to Admin</Accordion.Header>
                    <Accordion.Body>
                        <AdminConvert />
                    </Accordion.Body>
                </Accordion.Item>
                <Accordion.Item eventKey="1">
                    <Accordion.Header>Add a new product</Accordion.Header>
                    <Accordion.Body>
                        <AddNewProduct />
                    </Accordion.Body>
                </Accordion.Item>
                <Accordion.Item eventKey="2">
                    <Accordion.Header>All Products</Accordion.Header>
                    <Accordion.Body>
                        <AllProducts />
                    </Accordion.Body>
                </Accordion.Item>
                <Accordion.Item eventKey="3">
                    <Accordion.Header>All Orders</Accordion.Header>
                    <Accordion.Body>
                        <AllOrders />
                    </Accordion.Body>
                </Accordion.Item>
            </Accordion>
            :
            <Navigate to="/" />

    )
}