import { useState, useEffect, useContext } from 'react';
import { Row, Col } from 'react-bootstrap';
import { Navigate } from 'react-router-dom';
import ItemCard from '../components/ItemCard.js';
import UserContext from '../UserContext.js';

export default function MenProducts() {
    let status;

    const { logIn } = useContext(UserContext);

    const [items, setItems] = useState([]);

    useEffect(() => {
        fetch(`${process.env.REACT_APP_API_URL}/products/all-products/active`)
            .then(response => {
                status = response.status;
                return response.json()
            }).then(result => {
                if (status === 200) {
                    setItems(result.filter(item => {
                        return item.category === 'men'
                    }).map(item => {
                        return <Col lg={3} xs={12} className='my-3' key={item._id}>
                            <ItemCard product={item} />
                        </Col>

                    }))
                }
            })
    }, [])


    return (
        (logIn.isAdmin === true) ?
            <Navigate to="/" />
            :
            <Row className='d-flex justify-content-center mt-5'>
                <h1>For Men</h1>
                {items}
            </Row>


    )
}