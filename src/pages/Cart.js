import { useState, useEffect, useContext } from 'react';
import { Row, Col, Button, Table } from 'react-bootstrap';
import { Navigate, Link } from 'react-router-dom';
import CartItem from '../components/CartItem.js';
import UserContext from '../UserContext.js';

export default function Cart() {

    const { logIn } = useContext(UserContext);
    const [cartHasItems, setCartHasItems] = useState(false);
    const [items, setItems] = useState([]);

    const fetchData = () => {
        fetch(`${process.env.REACT_APP_API_URL}/cart/subtotal`, {
            headers: {
                "Authorization": `Bearer ${localStorage.getItem('token')}`
            }
        }).then(response => response.json())
            .then(result => {
                if (result.items === undefined) {
                    setCartHasItems(false);
                }
                else if (result.items.length > 0) {
                    setCartHasItems(true)
                    setItems(result.items.map(item => {
                        return <CartItem key={item.productId} cartItem={item} fetchData={fetchData} />
                    }))
                }
            })
    }

    useEffect(() => {
        fetchData();
    }, [])


    return (
        (logIn.id === null || logIn.isAdmin === true) ?
            <Navigate to="/" />
            :
            (cartHasItems === false) ?
                <>
                    <div className='text-center mt-5'>
                        <h1 className='my-5'>No Items in Cart.</h1>
                        <h2>Go to <a href="/men">Men's Catalog</a></h2>
                        <h2>Or</h2>
                        <h2>Go to <a href="/women">Women's Catalog</a></h2>
                    </div>
                </>
                :
                <>
                    <Row>
                        <h1 className='text-center mt-5'>.cart</h1>
                        <Col lg={{ span: 8, offset: 2 }}>
                            <Table className='text-center align-middle mt-5 border border-muted' size="sm" hover responsive>
                                <thead>
                                    <tr>
                                        <th colSpan={2}>Product</th>
                                        <th>Price</th>
                                        <th colSpan={3}>Quantity</th>
                                        <th>Subtotal</th>
                                        <th>Actions</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    {items}
                                </tbody>
                            </Table>
                        </Col>
                    </Row>
                    <Row>
                        <Col lg={{ span: 8, offset: 2 }} className='mt-3'>
                            <div className="d-grid gap-2">
                                <Button variant="primary" size="lg" as={Link} to="/checkout">
                                    Proceed To Checkout
                                </Button>
                            </div>
                        </Col>
                    </Row>
                </>

    )
}